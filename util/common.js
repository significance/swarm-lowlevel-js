var keccak = require('keccak');

module.exports = {
	uint8ToHex: uint8ToHex,
	toDigest32: toDigest32,
	hexToArray: hexToArray,
	arrayToHex: arrayToHex,
}

function uint8ToHex(b) {
	let hex = Array.prototype.map.call(b, function(x) {
		return ('00' + x.toString(16)).slice(-2);
	});
	return hex.join('');
}

function toDigest32(s, format='hex') {
	let hasher = keccak('keccak256');
	hasher.update(s);
	if (format == 'hex') {
		return hasher.digest('hex');	
	} else {
		return hasher.digest();
	}	
}

function hexToArray(data) {
	let databuf = new ArrayBuffer(data.length / 2);
	let uintdata = new Uint8Array(databuf);
	for (var i = 0; i < uintdata.length; i++) {
		uintdata[i] = parseInt(data.substring(i*2,(i*2)+2), 16);
	}
	return uintdata;
}

function arrayToHex(data) {
	let hexout = '';
	data.forEach(function(n) {
		let h = n.toString(16);
		if (h.length == 1) {
			h = "0" + h;
		}
		hexout += h;
	});
	return hexout;
}
