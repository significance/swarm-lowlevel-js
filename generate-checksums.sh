#!/usr/bin/env sh

files=(chunk/content.js chunk/index.js bmt/bmt.js bmt/index.js file/file.js file/index.js file/join.js feed/feed.js feed/index.js unsafewallet/unsafewallet.js unsafewallet/index.js soc/soc.js soc/index.js util/common.js tools/mine.js tools/unsafewallet.js test_manual/bmt_test.js test_manual/file_test.js test_manual/join_test.js test/01_chunk.js test/10_soc.js dist/index.js);
for f in ${files[@]}; do
	sha256sum $f
done
