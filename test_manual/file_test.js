let filehasher = require("../file/file");
let util = require('../util/common');

let chunkSize = filehasher.ChunkSize;

// attempt test vectors borrowed from go-swarm
let dataLengths = [
		31,			// 0
		32,                     // 1
		33,                     // 2
		63,                     // 3
		64,                     // 4
		65,                     // 5
		chunkSize,              // 6
		chunkSize + 31,         // 7
		chunkSize + 32,         // 8
		chunkSize + 63,         // 9
		chunkSize + 64,         // 10
		chunkSize * 2,          // 11
		chunkSize*2 + 32,       // 12
		chunkSize * 128,        // 13
		chunkSize*128 + 31,     // 14
		chunkSize*128 + 32,     // 15
		chunkSize*128 + 64,     // 16
		chunkSize * 129,        // 17
		chunkSize * 130,        // 18
		chunkSize * 128 * 128,  // 19
		chunkSize*128*128 + 32, // 20
];

let expected = [
		"ece86edb20669cc60d142789d464d57bdf5e33cb789d443f608cbd81cfa5697d", // 0
		"0be77f0bb7abc9cd0abed640ee29849a3072ccfd1020019fe03658c38f087e02", // 1
		"3463b46d4f9d5bfcbf9a23224d635e51896c1daef7d225b86679db17c5fd868e", // 2
		"95510c2ff18276ed94be2160aed4e69c9116573b6f69faaeed1b426fea6a3db8", // 3
		"490072cc55b8ad381335ff882ac51303cc069cbcb8d8d3f7aa152d9c617829fe", // 4
		"541552bae05e9a63a6cb561f69edf36ffe073e441667dbf7a0e9a3864bb744ea", // 5
		"c10090961e7682a10890c334d759a28426647141213abda93b096b892824d2ef", // 6
		"91699c83ed93a1f87e326a29ccd8cc775323f9e7260035a5f014c975c5f3cd28", // 7
		"73759673a52c1f1707cbb61337645f4fcbd209cdc53d7e2cedaaa9f44df61285", // 8
		"db1313a727ffc184ae52a70012fbbf7235f551b9f2d2da04bf476abe42a3cb42", // 9
		"ade7af36ac0c7297dc1c11fd7b46981b629c6077bce75300f85b02a6153f161b", // 10
		"29a5fb121ce96194ba8b7b823a1f9c6af87e1791f824940a53b5a7efe3f790d9", // 11
		"61416726988f77b874435bdd89a419edc3861111884fd60e8adf54e2f299efd6", // 12
		"3047d841077898c26bbe6be652a2ec590a5d9bd7cd45d290ea42511b48753c09", // 13
		"e5c76afa931e33ac94bce2e754b1bb6407d07f738f67856783d93934ca8fc576", // 14
		"485a526fc74c8a344c43a4545a5987d17af9ab401c0ef1ef63aefcc5c2c086df", // 15
		"624b2abb7aefc0978f891b2a56b665513480e5dc195b4a66cd8def074a6d2e94", // 16
		"b8e1804e37a064d28d161ab5f256cc482b1423d5cd0a6b30fde7b0f51ece9199", // 17
		"59de730bf6c67a941f3b2ffa2f920acfaa1713695ad5deea12b4a121e5f23fa1", // 18
		"522194562123473dcfd7a457b18ee7dee8b7db70ed3cfa2b73f348a992fdfd3b", // 19
		"ed0cc44c93b14fef2d91ab3a3674eeb6352a42ac2f0bbe524711824aae1e7bcc", // 20
];


let zeros = Buffer.alloc(filehasher.ChunkSize * filehasher.Branches * filehasher.Branches + filehasher.ChunkSize);
let data = Buffer.alloc(filehasher.ChunkSize * filehasher.Branches * filehasher.Branches + filehasher.ChunkSize);
for (let i = 0; i < data.length; i++) {
	data[i] = i%255;
}

let h = new filehasher.Splitter();


////////////////////////////
// write one chunk of data
console.debug("test single chunk");
let buf = data.slice(0, chunkSize);
h.update(0, buf);

// check that sum has been written at beginning
if (h.cursors[1] != filehasher.SectionSize) {
	throw "single chunk write; expected cursor level1: 32, got " + h.cursors[1];
}

// check that sum is correct
buf = h.buffer.slice(0, filehasher.SectionSize);
bufHex = util.uint8ToHex(Array.from(buf));
correctHex = "c10090961e7682a10890c334d759a28426647141213abda93b096b892824d2ef"
if (bufHex != correctHex) {
	throw "single chunk write ref; expected " + correctHex + ", got " + bufHex;
}


////////////////////////////
// write balanced tree
console.debug("test balanced tree");
h.reset();
let dataLength = filehasher.ChunkSize * filehasher.Branches;
for (let i = 0; i < dataLength; i += filehasher.ChunkSize) {
	buf = data.slice(i, i + filehasher.ChunkSize);
	h.update(0, buf);
}

// check cursors
if (h.cursors[0] != 32) {
	throw "balanced tree write; expected cursor level 0; expected 32, got " + h.cursors[0];
}
if (h.cursors[1] != 32) {
	throw "balanced tree write; expected cursor level 1: 32, got " + h.cursors[1];
}
if (h.cursors[2] != 32) {
	throw "balanced tree write; expected cursor level 2: 32, got " + h.cursors[2];
}
if (h.cursors[3] != 0) {
	throw "balanced tree write; expected cursor level 3: 0, got " + h.cursors[3];
}

// check that sum is correct
buf = h.buffer.slice(0, filehasher.SectionSize);
bufHex = util.uint8ToHex(Array.from(buf));
correctHex = "3047d841077898c26bbe6be652a2ec590a5d9bd7cd45d290ea42511b48753c09";
if (bufHex != correctHex) {
	throw "balanced tree write ref; expected " + correctHex + ", got " + bufHex;
}


////////////////////////////
// write two chunks and digest
console.debug("test two chunks");
h.reset();
buf = data.slice(0, chunkSize);
h.update(0, buf);
buf = data.slice(chunkSize, chunkSize+chunkSize);
h.update(0, buf);
let ref = h.digest();

// check that sum has been written at beginning
if (h.cursors[2] != filehasher.SectionSize) {
	throw "two chunks write; expected cursor level 2: 32, got " + h.cursors[2];
}

// check that sum of the second chunk is correct
buf = h.buffer.slice(0, filehasher.SectionSize);
bufHex = util.uint8ToHex(Array.from(buf));
correctHex = "29a5fb121ce96194ba8b7b823a1f9c6af87e1791f824940a53b5a7efe3f790d9";
if (bufHex != correctHex) {
	throw "two chunks write ref; expected " + correctHex + ", got " + bufHex;
}


////////////////////////////
// write one chunk plus one segment and digest
console.debug("test one chunk plus segment");
h.reset();
buf = data.slice(0, chunkSize);
h.update(0, buf);
buf = data.slice(chunkSize, chunkSize + filehasher.SectionSize);
h.update(0, buf);
ref = h.digest();

// check that sum has been written at beginning
if (h.cursors[2] != filehasher.SectionSize) {
	throw "one chunk plus segment write; expected cursor level 2: 32, got " + h.cursors[2];
}


// check that sum of the second chunk is correct
buf = h.buffer.slice(0, filehasher.SectionSize);
bufHex = util.uint8ToHex(Array.from(buf));
correctHex = "73759673a52c1f1707cbb61337645f4fcbd209cdc53d7e2cedaaa9f44df61285";
if (bufHex != correctHex) {
	throw "one chunk plus segment write ref; expected " + correctHex + ", got " + bufHex;
}


////////////////////////////
// write one chunk plus one part segment and digest
console.debug("test one chunk plus one part segment");
h.reset();
buf = data.slice(0, chunkSize);
h.update(0, buf);
buf = data.slice(chunkSize, chunkSize + filehasher.SectionSize - 1);
h.update(0, buf);
ref = h.digest();

// check that sum has been written at beginning
if (h.cursors[2] != filehasher.SectionSize) {
	throw "one chunk plus part segment write; expected cursor level 2: 32, got " + h.cursors[2];
}


// check that sum of the second chunk is correct
buf = h.buffer.slice(0, filehasher.SectionSize);
bufHex = util.uint8ToHex(Array.from(buf));
correctHex = "91699c83ed93a1f87e326a29ccd8cc775323f9e7260035a5f014c975c5f3cd28";
if (bufHex != correctHex) {
	throw "one chunk plus part segment write ref; expected " + correctHex + ", got " + bufHex;
}


////////////////////////////
// write balanced tree plus one section
console.debug("test balanced tree with dangling section");
h.reset();
dataLength = filehasher.ChunkSize * filehasher.Branches + filehasher.SectionSize;
for (let i = 0; i < dataLength; i += filehasher.ChunkSize) {
	let l = filehasher.ChunkSize;
	if (dataLength - i < filehasher.ChunkSize) {
		l = dataLength - i;
	}
	buf = data.slice(i, i+l);
	h.update(0, buf);
}
ref = h.digest();

// check that sum has been written at beginning
if (h.cursors[3] != filehasher.SectionSize) {
	throw "balanced tree with dangling section write; expected cursor level 3: 32, got " + h.cursors[3];
}

// check that sum is correct
refHex = util.uint8ToHex(Array.from(ref));
correctHex = "485a526fc74c8a344c43a4545a5987d17af9ab401c0ef1ef63aefcc5c2c086df"
if (refHex != correctHex) {
	throw "balanced tree with dangling section write ref; expected " + correctHex + ", got " + refHex;
}


////////////////////////////
// write balanced tree plus one chunk
console.debug("test balanced tree with dangling chunk");
h.reset();
dataLength = filehasher.ChunkSize * filehasher.Branches + filehasher.ChunkSize;
for (let i = 0; i < dataLength; i += filehasher.ChunkSize) {
	buf = data.slice(i, i+filehasher.ChunkSize);
	h.update(0, buf);
}
ref = h.digest();

// check that sum has been written at beginning
if (h.cursors[3] != filehasher.SectionSize) {
	throw "balanced tree with dangling chunk write; expected cursor level 3: 32, got " + h.cursors[3];
}

// check that sum is correct
refHex = util.uint8ToHex(Array.from(ref));
correctHex = "b8e1804e37a064d28d161ab5f256cc482b1423d5cd0a6b30fde7b0f51ece9199"
if (refHex != correctHex) {
	throw "balanced tree with dangling chunk write ref; expected " + correctHex + ", got " + refHex;
}


////////////////////////////
// main hasher entrypoint
console.debug("test main entry with vectors");
for (let i = 0; i < dataLengths.length; i++) {
	h.reset();
	console.debug("vector " + i + ":" + dataLengths[i]);
	buf = data.slice(0, dataLengths[i]);
	let ref = h.split(buf);
	let refHex = util.uint8ToHex(Array.from(ref));
	if (refHex != expected[i]) {
		throw "vector; expected " + expected[i] + ", got " + refHex;
	}
}
