let fs = require('fs');

let bmt = require('../bmt/bmt');
let util = require('../util/common');

let data = fs.readFileSync(0);
let h = new bmt.Hasher()
h.ResetWithLength(data.length)
h.Write(data)
console.log(util.uint8ToHex(h.Sum()))
