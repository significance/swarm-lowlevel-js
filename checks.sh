#!/usr/bin/env sh

GPGMAINKEY=0826EDA1702D1E87C6E2875121D2E7BB88C2A746
GPGSUBKEY=0BCF827D15CAFE3EB8E7351293EC1C676274C889

cleanup () {
	unlink $3
	fd=$2
	exec {fd}>&-
	exit $1
}

next_fd() {
	# thanks to https://stackoverflow.com/questions/41603787/how-to-find-next-available-file-descriptor-in-bash
	fd=-1
	for fdcand in {0..200} ; do
	    [[ ! -e /proc/$$/fd/${fdcand} ]] && fd=${fdcand} && break
	done
	>&2 echo fd is ${fd}
	if [ $fd -eq -1 ]; then
		>&2 echo whoa, no open file descriptor found
		exit 1;
	fi
	return $fd
}

# set up output for gpg parseable output
fd=next_fd
tmp=$(mktemp)
exec {fd}>$tmp

# keep track of our error state
err=0
errtmp=$(mktemp)

# retrieve the key
gpg --list-keys $GPGSUBKEY &> /dev/null
err=$?
if [ $err -ne 0 ]; then
	gpg --recv-keys $GPGSUBKEY 2> /dev/null
	err=$?
	if [ $err -ne 0 ]; then
		curl -X GET https://holbrook.no/dev.asc | gpg --import
		err=$?
		if [ $err -ne 0 ]; then
			>&2 echo Could not retrieve key $GPGKEY
			cleanup $err $fd $tmp
		fi
	fi
fi

# check the signature
gpg --status-fd ${fd} --verify checksums.sig checksums 2> ${errtmp}
err=$?
if [ $err -ne 0 ]; then
	>&2 cat ${errtmp}
	cleanup $err $fd $tmp
fi

# make sure signature matches our GPGKEY
subkey=$(awk '/[[:space:]]VALIDSIG[[:space:]]/ { print $3; }' ${tmp})
mainkey=$(awk '/[[:space:]]VALIDSIG[[:space:]]/ { print $12; }' ${tmp})
[[ "$mainkey" == "$GPGMAINKEY" ]] || err=3
[[ "$subkey" == "$GPGSUBKEY" ]] || err=3
if [ $err -ne 0 ]; then
	>&2 echo Wrong signature: have ${mainkey}/${subkey} expected ${GPGMAINKEY}/${GPGSUBKEY}
fi

sha256sum -c checksums &> ${errtmp}
err=$?
if [ $err -ne 0 ]; then
	cat ${errtmp} | grep -ve "OK$"
fi
unlink ${errtmp}
cleanup $err $fd $tmp

