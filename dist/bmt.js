var keccak = require('keccak');

var digestLength = 32;
var sectionSize = digestLength;
var twoSections = sectionSize * 2;
var branches = 128;
var chunkSize = branches * sectionSize;

module.exports = {
	Hasher: Hasher,
	SectionSize: sectionSize,
	Branches: branches,
	ChunkSize: chunkSize,
};

function Hasher() {
	this.span = new Buffer.allocUnsafe(8);
	this.length = 0;
	this.buffer = new Buffer.alloc(chunkSize);
}

Hasher.prototype.ResetWithLength = function(n) {
	if (typeof(n) == 'number') {
		n = BigInt(n);
	}
	if (typeof(n) !== 'bigint') {
		throw "not a valid bigint or convertible value";
	}
	for (let i = 0; i < chunkSize; i++) {
		this.buffer[i] = 0;
	}
	this.length = 0;
	this.span.writeBigUInt64LE(n);
}

Hasher.prototype.Write = function(b) {
	if (typeof b != "object") { 
		throw "expected buffer, got " + typeof b;
	}
	if (this.length + b.length > chunkSize) {
		throw "exceeded max size " + chunkSize;
	}
	for (let i = 0; i < b.length; i++) {
		this.buffer[this.length] = b[i];
		this.length++;
	}
}

Hasher.prototype.Sum = function() {
	let tmpBuf = Buffer.alloc(twoSections);
	for (let i = chunkSize; i > sectionSize; i /= 2) {
		for (let j = 0; j < i; j += twoSections) {
			let hasher = keccak("keccak256");
			for (let k = 0; k < twoSections; k++) {
				tmpBuf[k] = this.buffer[j+k];
			}
			hasher.update(tmpBuf);
			let ref = hasher.digest();
			let offset = j/2;
			for (let k = 0; k < sectionSize; k++) {
				this.buffer[offset] = ref[k];
				offset++;
			}
		}
	}
	let hasher = keccak("keccak256");
	let refBuf = Buffer.alloc(sectionSize);
	hasher.update(Buffer.from(this.span));
	for (let k = 0; k < refBuf.length; k++) {
		refBuf[k] = this.buffer[k];
	}
	hasher.update(refBuf);
	return hasher.digest();
}
