let file = require('./file');
let join = require('./join');

module.exports = {
	chunkSize: file.ChunkSize,
	sectionSize: file.SectionSize,
	branches: file.Branches,
	Splitter: file.Splitter,
	Joiner: join.Joiner,
};
