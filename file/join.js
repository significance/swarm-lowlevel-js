let bmt = require('../bmt/bmt');
let util =require('../util/common');

module.exports = {
	Joiner: Joiner,
};

// inCallback interface implement get
function Joiner(inCallback, outCallback) {
	this.levels = 0;
	this.cursors = new Array(9);
	this.retrieved = 0;
	this.buffer = new Uint8Array(bmt.ChunkSize*2*9);
	this.inCallback = inCallback;
	this.outCallback = outCallback;
	this.counts = new Array(9);
	this.size = -1;
	this.reset();
}

Joiner.prototype.get = function(hash) {
	let buf = this.inCallback(hash);
	let view = new DataView(buf);
	let size = view.getUint32(0, true);
	return {
		size: size,
		buf: buf.slice(8, buf.length),
	};

}

Joiner.prototype.join = function(hash) {
	obj = this.get(hash);
	this.size = obj.size;
	this.retrieve(0, this.size, hash, obj.buf); // here level 0 is root
}

Joiner.prototype.retrieve = function(lvl, size, hash, buf) {
	if (size <= bmt.ChunkSize) {
		this.outCallback({
			offset: this.retrieved,
			total: size,
			reference: hash,
			data: new Uint8Array(buf),
		});
		this.retrieved += size;
		return true;
	}
	for (let i = 0; i < buf.byteLength; i += bmt.SectionSize) {
		let newHash = buf.slice(i, i+bmt.SectionSize)
		let newHashArray = new Uint8Array(newHash);
		let newHashHex = util.uint8ToHex(newHashArray);
		let newObj = this.get(newHashHex);
		this.retrieve(lvl+1, newObj.size, newHash, newObj.buf);
	}
	
}

Joiner.prototype.reset = function() {
	for (let i = 0; i < 9; i++) {
		this.cursors[i] = 0;
		this.counts[i] = 0;
	}
	this.length = 0;
	this.levels = 0;
}
