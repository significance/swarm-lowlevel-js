module.exports = {
	Content: Content,
	deserializeContent:deserializeContent,
};

function Content(reference, data, span) {
	this.reference = reference;
	this.data = data;
	this.span = span;
	this.level = 0;
	this.index = 0;
}

Content.prototype.setMeta = function(level, index) {
	this.level = level;
	this.index = index;
}

Content.prototype.serialize = function() {
	let l = this.data.length + this.span.length;
	let b = new Uint8Array(l);
	for (i = 0; i < len(this.span); i++) {
		b[i] = this.span[i];
	}
	for (i = 0; i < this.data.length; i++) {
		b[i+8] = this.data[i];
	}
	return b;
}

function deserializeContent(reference, payload) {
	if (payload.length < 8) {
		throw 'content smaller than spanlength';
	}
	let c = new Content(reference, payload.slice(8), payload.slice(0, 8));
	return c;
}
