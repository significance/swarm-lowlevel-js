let soc = require('../soc');
let file = require('../file');
let bmt = require('../bmt');
let assert = require('assert');
let wallet = require('../unsafewallet');
let common = require('../util/common');
let secp256k1 = require('secp256k1');

let soc_id = new Uint8Array(32);
let bmtHashOfFoo = "2387e8e7d8a48c2a9339c97c1dc3461a9a7aa07e994c5cb8b38fd7c1b3e6ea48";
let topicHex = "6974776173746865626573746f6674696d6573776f7273746f6674696d65730a";
let topic = common.hexToArray(topicHex);
fooData = new Uint8Array([0x66, 0x6f, 0x6f]);

let ch = undefined;
let cb = function(_ch) {
	ch = _ch;	
};
let f = new file.Splitter(cb);
f.split(fooData);
let pk = new Uint8Array(32);
pk[0] = 0x2a;


describe('soc', () => {

	it('create_with_array', function() {
		new soc.Soc(topic, undefined, undefined);
		let err = false
		try {
			new soc.Soc(topic.slice(1), undefined, undefined);
		} catch(e) {
			err = true
		}
		assert(err);
	});

	it('create_with_hex', function() {
		new soc.Soc(bmtHashOfFoo, undefined, undefined);
		let err = false
		try {
			new soc.Soc(bmtHashOfFoo.substring(2), undefined, undefined);
		} catch(e) {
			err = true
		}
		assert(err);
	});

	it('create_then_chunk', function() {
		let s = new soc.Soc(soc_id, undefined, undefined);
		let f = new file.Splitter(s.setChunk);
		f.split(fooData);
		let content_address_hex = common.uint8ToHex(s.chunk.reference);
		assert.equal(content_address_hex, bmtHashOfFoo);
	});

	it('create_from_data', function() {
		let soc_id = new Uint8Array(32);
		let s = soc.newFromData(soc_id, fooData, undefined);
		let content_address_hex = common.uint8ToHex(s.chunk.reference);
		assert.equal(content_address_hex, bmtHashOfFoo);
	});

	it('create_with_chunk', function() {
		let soc_id = new Uint8Array(32);
		let s = new soc.Soc(soc_id, ch, undefined);
		let content_address_hex = common.uint8ToHex(s.chunk.reference);
		assert.equal(content_address_hex, bmtHashOfFoo);
	});

	it('sign', function() {
		let soc_id = new Uint8Array(32);
		let w = new wallet.Wallet(Buffer.from(pk));
		let s = new soc.Soc(soc_id, ch, w);
		s.sign();
		assert.notEqual(s.signature, undefined);

		let digest = soc.digestToSign(soc_id, ch.reference);
		let publicKeyBuffer = Buffer.from(w.getPublicKey('binary'));
		let recoveredPublicKeyBuffer = secp256k1.recover(digest, s.signature.signature, s.signature.recovery, false);
		assert.deepEqual(recoveredPublicKeyBuffer, publicKeyBuffer);
	});

	it('soc_address', function() {
		let soc_id = new Uint8Array(32);
		let s = new soc.Soc(soc_id, ch, undefined);
		let w = new wallet.Wallet(Buffer.from(pk));
		s.setOwnerAddress(w.getAddress('binary'));
		console.debug("soc address", s.getAddress());
	});

	it('serialize', function() {
		// TODO: add buffer copy
		let soc_id = topic; //new Uint8Array(32);
		let w = new wallet.Wallet(Buffer.from(pk));
		let cb = console.debug;
		let s = new soc.Soc(soc_id, ch, w, cb);
		s.sign(cb);

		let z = s.serializeData();

		let cursor = 0;
		let id_deserialized = z.slice(cursor, cursor + soc.idLength);
		assert.deepEqual(id_deserialized, soc_id);

		cursor += soc.idLength;
		assert.equal(z[cursor], s.signature.recovery+31);

		cursor++;
		let signature_deserialized = z.slice(cursor, cursor + soc.signatureLength-1);
		assert.deepEqual(signature_deserialized, s.signature.signature);

		cursor += soc.signatureLength-1;
		let span_deserialized = z.slice(cursor, cursor + bmt.spanSize);
		assert.deepEqual(span_deserialized, ch.span);

		cursor += bmt.spanSize;
		let payload_deserialized = z.slice(cursor);
		assert.deepEqual(payload_deserialized, ch.data);
	});

	it('deserialize', function() {
		let soc_id = topic; //new Uint8Array(32);
		let w = new wallet.Wallet(Buffer.from(pk));
		let cb = console.debug;
		let s = new soc.Soc(soc_id, ch, w, cb);
		s.sign(cb);
		let z = s.serializeData();
		console.debug('zzzz', z);
		let sr = soc.deserialize(z);
	
		s.signer = undefined;
		s.callback = undefined;
		// TODO: add function to soc for check if match
		assert.deepEqual(sr.id, s.id);
		assert.deepEqual(sr.signature, s.signature);
		assert.deepEqual(sr.owner_address, s.owner_address);
		assert.deepEqual(sr.chunk, s.chunk);

	});

	it('validate', function() {
		let soc_id = topic; //new Uint8Array(32);
		let w = new wallet.Wallet(Buffer.from(pk));
		let cb = console.debug;
		let s = new soc.Soc(soc_id, ch, w, cb);
		s.sign();
		let r = s.getAddress();
		let z = s.serializeData();
		let sr = soc.deserialize(z);
		assert(sr.validate(r));
	});

	it('vector', function() {
		let soc_id = topic;
		let lpkHex = "b34f5aa269ff5ec30af5631329f9e057b45043d0c4974a0844617494d70c1336";
		let lpk = common.hexToArray(lpkHex);
		let w = new wallet.Wallet(Buffer.from(lpk));
		let cb = console.debug;
		let s = new soc.Soc(soc_id, ch, w, cb);

	
		let addressCorrect = new Uint8Array([27, 27, 212, 16, 143, 3, 40, 119, 75, 170, 41, 27, 96, 136, 172, 170, 197, 135, 158, 60, 235, 255, 57, 244, 66, 234, 40, 129, 94, 86, 234, 96]);
			
			
		let dataCorrect = new Uint8Array([105, 116, 119, 97, 115, 116, 104, 101, 98, 101, 115, 116, 111, 102, 116, 105, 109, 101, 115, 119, 111, 114, 115, 116, 111, 102, 116, 105, 109, 101, 115, 10, 31, 255, 188, 79, 242, 69, 12, 156, 216, 216, 77, 111, 176, 179, 21, 71, 142, 212, 226, 208, 5, 33, 44, 166, 173, 64, 3, 46, 116, 96, 96, 140, 93, 81, 147, 35, 17, 156, 95, 131, 67, 19, 212, 170, 100, 81, 162, 69, 151, 208, 101, 243, 154, 73, 247, 19, 106, 195, 254, 45, 124, 158, 205, 64, 103, 3, 0, 0, 0, 0, 0, 0, 0, 102, 111, 111]);
			
		s.sign();

		console.debug('wwww', w.getAddress());
		assert.deepEqual(addressCorrect, s.getAddress());
		let dataActual = s.serializeData();
		for (let i = 32; i < 32+65; i++) {
			console.debug(i + ': ' + dataCorrect[i] + ' - ' + dataActual[i]);
		}
		assert.deepEqual(dataCorrect, dataActual)

	});
});
