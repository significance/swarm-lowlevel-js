let chunk = require('../chunk');
let assert = require('assert');


describe('chunk', () => {
	
	it('deserialize', () => {
		ref = new Uint8Array(32);
		payload = new Uint8Array(9);
		err = false;
		try {
			chunk.deserializeContent(ref, payload.slice(2));
		} catch {
			err = true;
		}
		assert(err);
		let ch = chunk.deserializeContent(ref, payload);

		assert.deepEqual(ref, ch.reference);
		assert.deepEqual(payload.slice(0, 8), ch.span);
		assert.deepEqual(payload.slice(8), ch.data);
		
	});
});
