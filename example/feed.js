#!/usr/bin/env node

// this is an example script showing a full round trip of creating a swarm feed update, adding it to swarm, then retrieving it
//
// first you probably need to read at least https://swarm-guide.readthedocs.io/en/latest/dapp_developer/index.html#http 
// otherwise the following may make little sense to you

let f = require('../feed/feed');
let unsafewallet = require('../unsafewallet/unsafewallet');
let common = require('../util/common');
let http = require('http');

// foo is the data and also the input for the hash constituing the private key (for convenience)
let foo = process.argv[2];
let fooDigest = common.toDigest32(foo, "bytes");
let privkey = undefined;
if (process.argv[3] != undefined) {
	privkey = Buffer.from(process.argv[3], 'hex');
} else {
	privkey = Buffer.from(fooDigest);
}
let wallet = new unsafewallet.Wallet(privkey);


// set up feed request, with topic and data
// we pass the wallet which will provide the key dependent part of the request
// (afterwards skip to the bottom to see where the http queries to the swarm node start)
let req = new f.Request(wallet);
req.setTopic(Buffer.alloc(32, 2));
req.setData(Buffer.from(foo));


// show what we have, which we of course never should do
console.log("privatekey: " + wallet.getPrivateKey());
console.log("publickey: " + wallet.getPublicKey());
console.log("address: " + wallet.getAddress());
console.log("curl: \"http://localhost:8500/bzz-feed:/?" + req.queryString(true) + "\"");


// handles http response for querying the next epoch
function handleMeta(res) {

	res.setEncoding('utf-8');
	let rawData = "";

	res.on('data', function(d) {
		rawData += d;
	});

	// when epoch is in, we sign the update
	// and output the query string
	// a post with the data ("foo") and this query string to a swarm node creates the update
	res.on('end', function() {

		let meta = JSON.parse(rawData);
		req.setEpoch(meta.epoch.level, meta.epoch.time);
		req.sign();

		let httpReq = http.request({

			method: "POST",
			hostname: "localhost",
			port: 8500,
			path: "/bzz-feed:/?" + req.queryString(),
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
				"Content-Length": foo.length
			}
			
		}, handlePost);

		httpReq.write(foo);
		httpReq.end();
		
	});

}


// handles http response for posting the update
function handlePost(res) {

	let rawData = "";	
	res.on('data', function(d) {
		rawData += d
	});
	res.on('end', function() {
		if (res.statusCode != 200) {
			console.error(res.statusCode, res.statusMessage, rawData);
			return false;
		}
		http.get({

			hostname: "localhost",
			port: 8500,
			path: "/bzz-feed:/?" + req.queryString(true)
		
		}, handleCheck);
	});
}


// handles http response for retrieving update after posting
function handleCheck(res) {
	let rawData = "";	
	res.on('data', function(d) {
		rawData += d
	});
	res.on('end', function() {
		if (rawData != foo) {
			console.error("got '" + rawData + "', expected '" + foo + "'");
			return false;
		}
		console.log("done", rawData);
		process.exit(0);
	});
}


// here start the swarm node interactions, whee
//
// query the "epoch" of the last update
// tip: if we knew this was a brand new feed, we could just use the defaults from the FeedRequest object
//
// should probably be some error handling here, but we indulge ourselves since it's only an example after all
http.get({
	hostname: "localhost",
	port: 8500, 
	path: "/bzz-feed:/?" + req.queryString(true) + "&meta=1",
}, handleMeta);
