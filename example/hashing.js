#!/usr/bin/env node

let bmt = require("../bmt/bmt");
let file = require("../file/file");
let util = require("../util/common");

let fooData = Buffer.from("foo")
let serialData = Buffer.alloc(bmt.ChunkSize * bmt.Branches);
for (let i = 0; i < bmt.ChunkSize * bmt.Branches; i++) {
	serialData[i] = i%255;
}

// bmt hasher usage
let bh = new bmt.Hasher();
//bh.ResetWithLength(3);
bh.reset();
bh.setSpan(fooData.length);
bh.Write(fooData);
let ref = bh.digest();
let refHex = util.uint8ToHex(Array.from(ref));
console.log("bmt('foo')\t" + refHex);


// filehasher usage
//let fh = new file.Hasher();
let fh = new file.Splitter();
//ref = fh.Hash(fooData);
ref = fh.split(fooData);
refHex = util.uint8ToHex(Array.from(ref));
console.log("filehasher('foo')\t" + refHex);


// bmt can only do up to 4096 bytes
let partData = serialData.slice(0, bmt.ChunkSize);
//bh.ResetWithLength(partData.length);
bh.reset();
bh.setSpan(partData.length);
//bh.Write(partData);
bh.write(partData);
//ref = bh.Sum();
ref = bh.digest();
refHex = util.uint8ToHex(Array.from(ref));
console.log("bmt(0..4095)\t" + refHex);


// filehasher can do any size
// and you can get the chunks back too
//fh = new file.Hasher(function(c) {
fh = new file.Splitter(function(c) {
	let data = c.data.slice(0, 8);
	let dataHex = util.uint8ToHex(Array.from(data));
	let refHex = util.uint8ToHex(c.reference);
	console.log("chunk index " + c.index + ", level " + c.level + ", ref " + refHex + ", data: " + dataHex + "...");
});
//ref = fh.Hash(serialData);
ref = fh.split(serialData);
refHex = util.uint8ToHex(Array.from(ref));
console.log("fh(0..524287):\t" + refHex);
